package models;

import java.util.Date;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Donation extends Model
{
  public long received;
  public String methodDonated;
  public Date date;
  
  @ManyToOne
  public User from;
  
  public Donation(User from, long received, String methodDonated)
  {
	  this.received = received;
	  this.methodDonated = methodDonated;
	  this.from = from;
	  date = new Date();
  }
}