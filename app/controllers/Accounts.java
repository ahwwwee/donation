package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Accounts extends Controller
{
  public static void signup()
  {
    render();
  }

  public static void login()
  {
    render();
  }

  public static void logout()
  {
    session.clear();
    index();
  }
  
  public static void index()
  {
    render();
  }
  
  public static void register(boolean usaCitizen, String firstName, String lastName, String email, String password)
  {
	  Logger.info(firstName + " " + lastName + " " + email + " " + password);
	  User user = new User (usaCitizen, firstName, lastName, email, password);
	  user.save();
      login();
  }

  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" +  password);

    User user = User.findByEmail(email);
    if ((user != null) && (user.checkPassword(password) == true))
    {
      Logger.info("Authentication successful");
      session.put("logged_in_userid", user.id);
      DonationController.index();
    }
    else
    {
      Logger.info("Authentication failed");
      login();  
    }
  }
  
  public static User getCurrentUser()
  {
	  String userId = session.get("logged_in_userid");
	  if(userId != null)
	  {
		  User logged_in_user = User.findById(Long.parseLong(userId));
		  return logged_in_user;
	  }
	  return null;
	  
      }
}